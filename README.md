# TimeLine for Biologists

A version of the TimeLine card game with events from the history of biology.

<div style="text-align: center;">
<img src="./misc/example_card_TimeLine_for_biologists.png" alt="Example of a card - Cliché ADN au rayons X" width="250" />
</div>

## Setup

### Requirements

- LaTeX
- LaTeX/pgf-ornaments
- Python
- pyyaml


### Generating the cards

Edit the `./events.yml` file to add new events.

Then run `./scripts/generate_cards.py` to generate the cards.

```bash
$ ./scripts/generate_cards.py -i ./events.yml -o ./tex/events.tex
```

Then, let's compile the LaTeX file.

```bash
$ cd tex && pdflatex deck.tex
```

Or 
    
```bash
$ ./run.sh
```

## License

This project source code is licensed under the LaTeX Project Public License v1.3c or later.

The content of the `./events.yml` file is licensed under the Creative Commons Attribution-ShareAlike 4.0 International License.

The images from `./media/cards/` have their own licenses.

