#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Generate cards from events.yml"""
import yaml
import click
import jinja2

def format_authors(authors_list):
	return ", ".join(authors_list)

@click.command()
@click.option('--input', '-i', 'input', default='events.yml', help='Input file')
@click.option('--output', '-o', 'output', default='cards.tex', help='Output file')
def generate_cards(input, output):
	with open(input, 'r') as file:
		events = yaml.safe_load(file)

	templateLoader = jinja2.FileSystemLoader(searchpath="./")
	templateEnv = jinja2.Environment(
		block_start_string='\BLOCK{',
		block_end_string='}',
		variable_start_string='\VAR{',
		variable_end_string='}',
		comment_start_string='\#{',
		comment_end_string='}',
		line_statement_prefix='%%',
		line_comment_prefix='%#',
		trim_blocks=True,
		autoescape=False,
		loader=templateLoader
	)
	TEMPLATE_FILE = "tex/templates/card.tex.jinja"
	template = templateEnv.get_template(TEMPLATE_FILE)
	outputText = ""
	for event in events['events']:
		if "authors" in event:
			event["people"] = format_authors(event["authors"])
		outputText += template.render(event)
	with open(output, "w") as f:
		f.write(outputText)

def main():
	generate_cards()


if __name__ == "__main__":
	main()
