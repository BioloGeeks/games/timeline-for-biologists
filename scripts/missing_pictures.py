#!/usr/bin/env python3 
"""
Check for missing pictures in `./media/cards/`
"""
import os
import yaml

def main():
    with open('events.yml', 'r') as f:
        events = yaml.safe_load(f)['events']
    for event in events:
        if 'image' in event:
            image = event['image']
            if not os.path.isfile(os.path.join('./media/cards/', image)):
                print(image)

if __name__ == '__main__':
    main()